package com.testbirds.connection;

import org.libvirt.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KvmConnection {
	private static Logger log = LoggerFactory.getLogger(KvmConnection.class);	
	
	public static Connect createConnection() throws LibvirtException{
		Connect kvmConnection = null;
		try{
			log.info("Creating connection...");
			kvmConnection = new Connect("qemu:///system");
		}catch( LibvirtException ex ){
			log.info("Failed Creating connection...");
			throw ex;
		}
		return kvmConnection;
	}
}
