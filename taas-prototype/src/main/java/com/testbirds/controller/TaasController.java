package com.testbirds.controller;

import java.util.UUID;

import org.libvirt.Connect;
import org.libvirt.Domain;
import org.libvirt.DomainInfo;
import org.libvirt.Error;
import org.libvirt.LibvirtException;
import org.libvirt.DomainInfo.DomainState;
import org.libvirt.Error.ErrorNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.testbirds.connection.KvmConnection;
import com.testbirds.shared.DomainInspector;
import com.testbirds.shared.SharedData;

@RestController("api/vm")
public class TaasController {
	
	private static Logger log = LoggerFactory.getLogger(TaasController.class);
	
	private Connect kvmConnection;
	
	public TaasController() throws LibvirtException{
		this.kvmConnection = KvmConnection.createConnection();
	}
	
	@RequestMapping(value="start/{macAddress}",method=RequestMethod.GET , produces=MediaType.TEXT_PLAIN_VALUE)
	public String start(@PathVariable(name="macAddress") String macAddress) throws LibvirtException{
		String vmStatus = null;
		String vmMacAddress = "'" + macAddress + "'";
		String vmName = "vmwindows_" + macAddress;
		
		try{
			UUID uuid = UUID.randomUUID();
			String windowsVmTemplate = SharedData.Vm.Windows.TEMPLATE;
			windowsVmTemplate.replace("$uUid", uuid.toString());
			windowsVmTemplate.replace("$vmName", vmName);
			windowsVmTemplate.replace("$vmMac", vmMacAddress);
			
			int domainCheckResult = DomainInspector.findExistingDomains(vmName);
			if (domainCheckResult == 1) {
				vmStatus = Error.ErrorNumber.VIR_ERR_DOM_EXIST.toString() + ": Already Running";
			} else if (domainCheckResult == 0) {
				vmStatus = ErrorNumber.VIR_ERR_DOM_EXIST.toString() + ": Already Defined";
			} else if (domainCheckResult == -1) {
				vmStatus = ErrorNumber.VIR_ERR_UNKNOWN.toString() + ": Unknown Error";
			} else {
				Domain domain = this.kvmConnection.domainDefineXML(windowsVmTemplate);
				domain.create();

				DomainState state = domain.getInfo().state;
				if (state == DomainState.VIR_DOMAIN_RUNNING) {
					vmStatus = "Vm Started";
				}
			}
		}catch( LibvirtException ex ){
			log.error(ex.getMessage());
			throw ex;
		}finally{
			if( kvmConnection != null ){
				kvmConnection.close();
			}else{
				log.info("Connection is null.");
			}
		}
		
		return vmStatus;
	}
	
	@RequestMapping(value="status/{macAddress}",method=RequestMethod.GET, produces=MediaType.TEXT_PLAIN_VALUE)
	public String getCurrentVmStatus(@PathVariable(name="macAddress") String macAddress) throws LibvirtException{
		String vmStatus = null;
		try{
			int[] intDomainList = kvmConnection.listDomains();
			if( intDomainList != null && intDomainList.length > 0 ){
				for( int idDomain : intDomainList ){
					Domain domain = kvmConnection.domainLookupByID(idDomain);
					String domainDescription = domain.getXMLDesc(0);
					if( domainDescription.contains(macAddress) ){
						DomainState domainState = domain.getInfo().state;
						if (domainState == DomainInfo.DomainState.VIR_DOMAIN_RUNNING) {
							vmStatus = "STARTED";
						} else if (domainState == DomainInfo.DomainState.VIR_DOMAIN_SHUTOFF) {
							vmStatus = "STOPPED";
						} else {
							vmStatus = "BOOTING";
						}
						
					}
				}
			}else{
				vmStatus = DomainState.VIR_DOMAIN_NOSTATE.toString();
			}
			
		}catch( LibvirtException ex ){
			log.error(ex.getMessage());
			throw ex;
		}finally{
			if( this.kvmConnection != null ){
				this.kvmConnection.close();
			}else{
				log.info("Connection is null.");
			}
		}
		return vmStatus;
	}
	
	@RequestMapping(value="stop/{macAddress}",method=RequestMethod.GET, produces=MediaType.TEXT_PLAIN_VALUE)
	public String stopVm(@PathVariable(name="macAddress") String macAddress) throws LibvirtException{
		String vmStatus = null;
		try{
			int[] intDomainList = kvmConnection.listDomains();
			String[] inactiveDomains = kvmConnection.listDefinedDomains();
			boolean isFoundAsActiveDomain = false;
			boolean isFoundAsInactiveDomain = false;
			if( intDomainList != null && intDomainList.length > 0 ){
				for( int idDomain : intDomainList ){
					Domain domain = kvmConnection.domainLookupByID(idDomain);
					String domainDescription = domain.getXMLDesc(0);
					if( domainDescription.contains(macAddress) ){
						isFoundAsActiveDomain = true;
						domain.shutdown();
						domain.undefine();
						vmStatus = "STOPPED";
						break;
					}
				}
			}else if( inactiveDomains != null && inactiveDomains.length > 0 ){
				for( String inactiveDomain : inactiveDomains ){
					Domain domain = kvmConnection.domainLookupByName(inactiveDomain);
					String domainDescription = domain.getXMLDesc(0);
					if( domainDescription.contains(macAddress) ){
						isFoundAsInactiveDomain = true;
						domain.undefine();
						vmStatus = "Inactive domain";
					}
				}
			}
			
			if( !isFoundAsActiveDomain && !isFoundAsInactiveDomain ){
				vmStatus = "Domain with mac address (" + macAddress + ") not found"; 
			}
		}catch( LibvirtException ex ){
			throw ex;
		}finally{
			if( this.kvmConnection != null ){
				this.kvmConnection.close();
			}else{
				log.info("Connection is null.");
			}
		}
		
		return vmStatus;
	}
	
}
