package com.testbirds.taasprototype;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages="com.testbirds.*")
public class TaasPrototypeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TaasPrototypeApplication.class, args);
	}
}
