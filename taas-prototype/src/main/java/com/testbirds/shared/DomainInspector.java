package com.testbirds.shared;

import org.libvirt.Connect;
import org.libvirt.Domain;
import org.libvirt.LibvirtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.testbirds.connection.KvmConnection;

public class DomainInspector {
	private static Logger log = LoggerFactory.getLogger(DomainInspector.class);
	
	public static int findExistingDomains(String domName) throws LibvirtException {
		Connect conn = null;
		int result = 0;
		
		System.setProperty("jna.library.path", "/usr/lib/");
		try {
			conn = KvmConnection.createConnection();
			int[] actDom = conn.listDomains();
			if (actDom.length < 1) {
				result = 2;
			} else {
				for (int i : actDom) {
					Domain name = conn.domainLookupByID(i);
					if ((name.getName()).equals(domName)) {
						if (name.isActive() == 1) {
							result = 1;
						} else if (name.isPersistent() == 1) {
							result = 0;
						} else if (name.isActive() == -1) {
							result = -1;
						}
					}
				}
			}

		} catch (LibvirtException e) {
			log.error(e.getMessage());
		} finally {
			if (conn != null) {
				conn.close();
			} else {
				log.info("Connection Error");
			}
		}
		return result;
	}	

}
